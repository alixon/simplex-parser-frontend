import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


import { AppConfig } from '../app.config';

@Injectable()
export class PhoneService {

  constructor(private _http: Http) {}

  getBrands(): Observable<any[]> {
      return this._http
          .get(`${AppConfig.backendUrl}/gsmarena/brands/`)
          .map( res => res.json() )
          .catch(this._handleError);
  }

  getModels(brandId): Observable<any[]> {
      return this._http
        .get(`${AppConfig.backendUrl}/gsmarena/models/${brandId}`)
        .map( response => response.json() )
        .catch(this._handleError);
  }

  getSpec(modelId): Observable<any[]> {
      return this._http
        .get(`${AppConfig.backendUrl}/gsmarena/spec/${modelId}`)
        .map( response => response.json() )
        .catch(this._handleError);
  }

  search(query): Observable<any[]> {
      query = encodeURIComponent(query);
      return this._http
          .get(`${AppConfig.backendUrl}/gsmarena/search/${query}`)
          .map( response => response.json() )
          .catch(this._handleError);
  }


  private _handleError(error: any) {
    let errMsg = error.message || error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    
    errMsg += "\r" + ( JSON.parse(error._body).message || '' );
    alert(errMsg);

    return Observable.throw(errMsg);
  }

}
