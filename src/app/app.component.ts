import { Component, OnInit, ViewChild } from '@angular/core';
import { SELECT_DIRECTIVES } from 'ng2-select';
import { PhoneService } from './services/phone.service';
import { AppConfig } from './app.config';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [ SELECT_DIRECTIVES ],
  providers: [ PhoneService ]
})
export class AppComponent implements OnInit {

  public brands: Object[] = [];
  public models: Object[] = [];

  public phoneHeader: string;
  public specification: Object[] = [];
  
  public searchListing: Object[] = [];
  public searchResultMessage: string;
  private _dataLoading: Boolean = false;

  @ViewChild("brandSelectBox") brandSelectBox;
  @ViewChild("modelSelectBox") modelSelectBox;

    constructor(private _phoneService: PhoneService) {}

    ngOnInit() {
      this._phoneService
        .getBrands()
        .subscribe(response => this.brands = response['data'] )
    }
    

    selectBrand(brand) {
      this.phoneHeader = brand.text;
      this.models = [];
      this.modelSelectBox.active = []
      this.specification = [];

      this._phoneService
        .getModels(brand.id)
        .subscribe(response => this.models = response['data'] );
    }


    selectModel(model) {
      this.phoneHeader = `${this.brandSelectBox.active[0].text} ${model.text}`;

      this._phoneService
        .getSpec(model.id)
        .subscribe(response => this.specification = response['data'] );
    }


    removeBrand(e) {
      this.phoneHeader = '';
      this.models = [];
      this.specification = [];
    }


    removeModel(e) {
      this.specification = [];
    }


    loadPhoneSpec(phone) {
      this.phoneHeader = phone.text;
      this.specification = [];
      this._dataLoading = true;

      this._phoneService
        .getSpec(phone.uri)
        .subscribe(response => {
          this.specification = response['data'];
          this._dataLoading = false;
        });

      return false;
    }


    searchSubmit(event) {
      let query = event.target.value;
      if( query.trim() ) {
        // unfortunately response does not contain phone title, let's use search query
        this.phoneHeader = event.target.value;
        
        this.specification = [];
        this.modelSelectBox.active = [];

        this._dataLoading = true;
        this._phoneService
          .search(query)
          .subscribe(response => {
            
            this._dataLoading = false;

            switch (response['type']) {
              case "listing":
                this.searchListing = response['data'];
                break;
              case "table":
                this.specification = response['data'];
                break;
              case "error":
                this.searchResultMessage = response["message"];
                setTimeout(
                  () => this.searchResultMessage = '', 
                  AppConfig.messageTimeout
                );
                break;
            }

          });

      }
    }


    isLoading() {
      return Boolean(
          (this.brandSelectBox.active.length 
            && this.modelSelectBox.active.length 
            && !this.specification.length) || this._dataLoading
        );

      
    }


}
